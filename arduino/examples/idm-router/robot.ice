// -*- mode: c++; coding: utf-8 -*-

module Robot {
    interface Station {
        void notifyBatteryLevel(byte level);
    };
};
