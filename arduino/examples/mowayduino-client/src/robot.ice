// -*- mode: c++; coding: utf-8 -*-

module Robot {
    interface Motion {
        void forward(int time);
        void backward(int time);
    };
};
